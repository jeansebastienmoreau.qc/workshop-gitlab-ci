# Gitlab CI - Projet Demo

## Flux du workshop

* Démarrage depuis la branche master
* Nous allons créer une branche accueillir nos exercices
    * `git checkout -b workshop && git push origin workshop`
* Définissons la branche `workshop` comme branche par défaut
* A chaque exercice (step), une branche avec la solution existe
    * Il est possible de faire un `git checkout step-N` pour avoir la solution
* Chaque exercice repart de la solution précédente
    * Exemple : Pour réaliser `step-2`, il vous faudra partir de la branche `step-1`
    * Si vous avez réussi les exercices, vous pouvez rester sur la branche courante.
    * Si vous n'avez pas réussi à réaliser l'exercice, vous pouvez réinitialiser la branche de travail avec la solution de l'étape : `git reset --hard origin/step-N`

## Les étapes

### Step 0 - Préparation

* Fork du projet
* Vérifications diverses
* Inviter moi sur le dépôt (jygastaud1)
* Création du fichier `.gitlab-ci.yml` à la racine

## Step 1 - Introduction à Script

#### Exercices 1.x

1.1 Afficher un `Hello World`
1.2 Vérifier que le fichier `index.html` existe
1.3 Ecrire un test en échec
1.4 Valider le pipeline malgré un test en echec
1.5 Désactiver un job

## Step 2 - Image

### Exercices 2.x

2.1 Définir une image par défaut à tous les jobs et tester qu’elle est bien utilisée
2.2 Surcharger l’image par défaut et vérifier que la nouvelle est bien utilisée

## Step 3 : Introduction aux variables

3.1 Faire évoluer nos scripts de test de fichier en variabilisant le nom du fichier
3.2 Redéfinir le nom fichier par défaut au niveau d’un job et définir plusieurs variables
3.3 Voir toute la liste des variables et le détail de l'exécution
3.4 Afficher le numéro du commit et la branche de référence avec les variables par défaut
3.5 Définir et afficher une variable de l’UI

## Step 4 - Stages

4.1 Répartir des jobs existants et les répartir des répartir dans les stages par défaut
4.2 Ajouter un nouveau stage

## Step 5 - needs

5.1 ajouter 3 needs sur 3 `stages`
5.2 ajouter un `sleep` sur un autre
5.3 voir que celui avec needs avance sans attendre que le `sleep` soit fini

## Step 6 - Before script et After script

6.1 Ajouter en `before_script` l’exécution d’un `composer install` qui se trouve dans le répertoire `apps/app2`
6.2 Créer un job qui va afficher le répertoire courant et lister les fichiers du dossier `vendor/`
6.3 Créer un 2nd job qui surcharge/annule le `before_script` global

## Step 7 - Only/Except et Rules

7.1 Définir que notre script de vérification des vendors (has-vendors) ne s'exécute que sur la branche `workshop`
7.2 Définir que notre de vérification de surcharge du `before_script` ne s’exécute que pas si la branche est `workshop`
7.3 Réécrire l’utilisation de `except` avec `rules`

## Step 8 - When

8.1 Ajouter un check `on_failure` sur un job existant et forcer un job précédent à fail
8.2 Créer un job déclenché manuellement

## Step 9 - templates et extends

9.1 Créer un template de script avec les 2 notations
9.2 Inclure ces templates dans un job

## Step 10 - Services

10.1 Déclarer un service mysql:8.0 et vérifier la connection à la base de données

## Step 11 - Artefacts & Reports dans une Merge Request

11.1 En utilisant l’application disponible dans `apps/app3`, créer un job qui va lancer l’exécution de la commande `pytest` dans une nouvelle branche
11.2 Créer une Merge Request de la branche vers `workshop`
11.3 Une fois le test exécuté, faire en sorte que le résultat s’affiche dans la Merge Request

## Step 12 - Include pour organiser ses jobs

12.1 Créer un fichier `build.gitlab-ci.yml` dans le dossier `.gitlab/templates/` et y définir un job
12.2 Inclure le fichier dans le fichier principal `gitlab-ci.yml`

## Step 13 - schedules - Les jobs scheduled

13.1 Créer un job schedulé sur la branche `workshop`
13.2 Faire en sorte qu’un job soit exclus de l’exécution en `schedules`

## Step 14 - Environments et Review Apps

14.1 Créer un job dans lequel sera déclaré un environnement et son url
14.2 Simuler la création d’une Review App
